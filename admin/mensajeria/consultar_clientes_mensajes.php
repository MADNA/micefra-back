<?php

include("../../conexion/conexion.php");
$conexion=conexion();

require_once '../../jwt/vendor/autoload.php';
use Firebase\JWT\JWT;

$headers = getallheaders();

@$token = getBearerToken($headers["Authorization"]);

if($token==""){
    $array = array("result"=>false,"msg"=>'No autorizado');
    $resultado=json_encode($array, JSON_UNESCAPED_UNICODE);
    echo $resultado;
    //header("HTTP/1.0 404 Not authorized");
    return;
}else{

    try {
        $jwt=$token;
        $key=$secret_key_admin;
        $data = JWT::decode($jwt, $key, array('HS256'));

		// $array = array("result"=>false,"msg"=>'');
        

        $sql = "CALL ConsultarClientesMensajes()";
        $query = mysqli_query($conexion, $sql);

        if(!$query){
			return;    
        }

        while($fila = mysqli_fetch_array($query)){
            $array[] = array(
                "id" =>  $fila["id"],
                "id_cliente" => $fila['id_cliente'],
                "usuario" => $fila['usuario'],
                "asunto" => $fila['asunto'],
                "mensaje" => $fila['mensaje']
            );
        }

        echo json_encode($array);
        
    } catch (\Exception $e) { // Also tried JwtException
        // echo $e->getMessage();
        $array = array("result"=>false,"msg"=>'No autorizado');
        $resultado=json_encode($array, JSON_UNESCAPED_UNICODE);
        echo $resultado;
        //header("HTTP/1.0 404 Not authorized");
        return;
    }

}
?>